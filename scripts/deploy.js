const hre = require("hardhat");

async function deployMockRng() {
  const MockRng = await hre.ethers.getContractFactory("MockRng");

  return await MockRng.deploy();
}

async function deployLottery(rng) {
  const Lottery = await hre.ethers.getContractFactory("Lottery");

  return Lottery.deploy(rng.address, 10000);
}

async function main() {
  const rng = await deployMockRng();
  const lottery = await deployLottery(rng);

  console.log("MockRNG deployed to:", rng.address);
  console.log("Lottery deployed to:", lottery.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });

// SPDX-License-Identifier: Unlicensed
pragma solidity ^0.8.0;

import { RngProvider } from "./Lottery.sol";

contract MockRng is RngProvider {
    mapping(address => uint256) requests;
    mapping(bytes32 => uint256) values;

    event NewRequest(address from, bytes32 id);

    function requestRandomNumber() external returns (bytes32) {
        requests[msg.sender] += 1;

        bytes32 id = keccak256(
            abi.encodePacked(msg.sender, requests[msg.sender])
        );

        emit NewRequest(msg.sender, id);

        return id;
    }

    function fulfill(bytes32 id) external view returns (uint256) {
        return values[id];
    }

    function setValue(bytes32 id, uint256 value) external {
        values[id] = value;
    }
}

// SPDX-License-Identifier: Unlicensed
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./SortitionSumTreeFactory.sol";

interface RngProvider {
    function requestRandomNumber() external returns (bytes32);

    function fulfill(bytes32 id) external view returns (uint256);
}

contract Lottery is Ownable {
    using SortitionSumTreeFactory for SortitionSumTreeFactory.SortitionSumTrees;

    enum LotteryState {
        Started,
        Finished
    }

    event NewDuration(uint256 duration);
    event LotteryStart(uint256 start, uint256 duration);
    event LotteryPayout(address indexed participant, uint256 value);
    event NewParticipant(address indexed participant, uint256 value);
    event LastParticipant(address indexed participant, uint256 value);

    uint256 private constant MAX_TREE_LEAVES = 5;

    RngProvider public rngProvider;
    LotteryState public state;

    SortitionSumTreeFactory.SortitionSumTrees internal trees;
    bytes32 internal treeKey;

    uint256 public startTimestamp;
    uint256 public finishTimestamp;
    uint256 public duration;
    bytes32 public rngRequestId;

    constructor(address _rngProvider, uint256 _duration) Ownable() {
        require(
            _rngProvider != address(0),
            "Random number generation provider must be valid address"
        );

        setDuration(_duration);

        rngProvider = RngProvider(_rngProvider);

        startLottery();
    }

    function setDuration(uint256 _duration) onlyOwner() public {
        require(_duration != 0, "Lottery duration must be positive");

        duration = _duration;

        emit NewDuration(duration);
    }

    function startLottery() internal {
        startTimestamp = block.timestamp;
        finishTimestamp = startTimestamp + duration;
        state = LotteryState.Started;
        rngRequestId = 0;

        treeKey = keccak256(abi.encodePacked(block.number));
        trees.createTree(treeKey, MAX_TREE_LEAVES);

        emit LotteryStart(startTimestamp, duration);
    }

    function addressToBytes32(address a) internal pure returns (bytes32 b) {
        b = bytes32(uint256(uint160(a)));
    }

    function addressFromBytes32(bytes32 b) internal pure returns (address a) {
        assembly {
            mstore(0, b)
            a := mload(0)
        }
    }

    function stakeOf(address participant) external view returns (uint256) {
        bytes32 id = addressToBytes32(participant);

        return trees.stakeOf(treeKey, id);
    }

    function update(address participant, uint256 amount) internal {
        if (state == LotteryState.Started) {
            require(amount > 0, "Some value is required");

            bytes32 id = addressToBytes32(participant);

            trees.set(treeKey, trees.stakeOf(treeKey, id) + amount, id);

            if (block.timestamp < finishTimestamp) {
                emit NewParticipant(participant, amount);
            } else {
                state = LotteryState.Finished;
                rngRequestId = rngProvider.requestRandomNumber();

                emit LastParticipant(participant, amount);
            }
        } else {
            require(msg.value == 0, "Unable to accept value: lottery already finished");

            uint256 n = rngProvider.fulfill(rngRequestId);
            bytes32 id = trees.draw(treeKey, n);
            address winner = addressFromBytes32(id);
            uint256 payout = address(this).balance;

            payable(winner).transfer(payout);

            emit LotteryPayout(winner, payout);

            delete trees.sortitionSumTrees[treeKey];

            startLottery();
        }
    }

    function submit() external payable {
        update(msg.sender, msg.value);
    }

    receive() external payable {
        update(msg.sender, msg.value);
    }
}

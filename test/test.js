const { expect } = require("chai");
const { BigNumber } = require("ethers");
const { ethers, waffle } = require("hardhat");

const provider = waffle.provider;

const ZERO_BYTES32 = "0x" + "00".repeat(32);

async function chainSleep(duration) {
    await ethers.provider.send("evm_increaseTime", [duration]);
    await ethers.provider.send("evm_mine");
}

async function getCurrentBlock() {
    const blockNumber = await ethers.provider.getBlockNumber();

    return await ethers.provider.getBlock(blockNumber);
}

describe("Lottery.constructor()", () => {
    let Lottery;
    let MockRng;

    let owner;

    beforeEach(async () => {
        [owner] = await ethers.getSigners();

        MockRng = await ethers.getContractFactory("MockRng");
        Lottery = await ethers.getContractFactory("Lottery", owner);
    });

    it("Should revert with message - Random number generation provider must be valid address", async () => {
        await expect(
            Lottery.deploy(ethers.constants.AddressZero, 100)
        ).to.be.revertedWith("Random number generation provider must be valid address");
    });

    it("Should revert with message - Lottery duration must be positive", async () => {
        const mockRngInstance = await MockRng.deploy();

        await expect(
            Lottery.deploy(mockRngInstance.address, 0)
        ).to.be.revertedWith("Lottery duration must be positive");
    });

    it("Should configure and emit events NewDuration and LotteryStart", async () => {
        const duration = 1000;

        const mockRngInstance = await MockRng.deploy();
        const lotteryInstance = await Lottery.deploy(mockRngInstance.address, duration);

        const block = await getCurrentBlock();

        const start = parseInt(await lotteryInstance.startTimestamp());
        const finish = parseInt(await lotteryInstance.finishTimestamp());

        await expect(lotteryInstance.deployTransaction)
            .to
                .emit(lotteryInstance, "NewDuration")
                .withArgs(duration);

        await expect(lotteryInstance.deployTransaction)
            .to
                .emit(lotteryInstance, "LotteryStart")
                .withArgs(block.timestamp, duration);

        expect(await lotteryInstance.owner()).to.equal(owner.address);
        expect(await lotteryInstance.rngProvider()).to.equal(mockRngInstance.address);
        expect(await lotteryInstance.state()).to.equal(0);
        expect(await lotteryInstance.duration()).to.equal(duration);
        expect(await lotteryInstance.rngRequestId()).to.equal(ZERO_BYTES32);
        expect(finish - start).to.equal(duration);
    })
});

describe("Lottery.stakeOf() / Lottery.submit() / Lottery.receive() / Lottery.setDuration()", () => {
    const LOTTERY_DURATION = 10000;
    const LOTTERY_TIMEOUT = LOTTERY_DURATION + 1;
    const TEST_AMOUNT = ethers.utils.parseEther("5");
    const PARTICIPANTS_TOTAL = 5;

    let owner;
    let participantOne;
    let participantTwo;

    let mockRngInstance;
    let lotteryInstance;

    let accounts;

    beforeEach(async () => {
        await provider.send("hardhat_reset")

        accounts = await ethers.getSigners();

        [owner, participantOne, participantTwo] = accounts;

        const MockRng = await ethers.getContractFactory("MockRng");
        const Lottery = await ethers.getContractFactory("Lottery", owner);

        mockRngInstance = await MockRng.deploy();
        lotteryInstance = await Lottery.deploy(mockRngInstance.address, LOTTERY_DURATION);
    });

    it("sumbit(): Should revert with message - Some value is required", async () => {
        await expect(
            lotteryInstance.connect(participantOne).submit()
        ).to.be.revertedWith("Some value is required");
    });

    it("sumbit(): Should emit NewParticipant event and add ether to contract balance", async () => {
        await expect(
            lotteryInstance.connect(participantOne).submit({ value: TEST_AMOUNT })
        ).to
            .emit(lotteryInstance, "NewParticipant")
            .withArgs(participantOne.address, TEST_AMOUNT);

        expect(await provider.getBalance(lotteryInstance.address)).to.equal(TEST_AMOUNT);
    });

    it("sumbit(): Should emit LastParticipant event", async () => {
        await chainSleep(LOTTERY_TIMEOUT);

        await expect(
            lotteryInstance.connect(participantOne).submit({ value: TEST_AMOUNT })
        ).to
            .emit(lotteryInstance, "LastParticipant")
            .withArgs(participantOne.address, TEST_AMOUNT);
    });

    it("sumbit(): Should revert with message - Unable to accept value: lottery already finished", async () => {
        await chainSleep(LOTTERY_TIMEOUT);

        await lotteryInstance.connect(participantOne).submit({ value: TEST_AMOUNT });

        await expect(
            lotteryInstance.connect(participantTwo).submit({ value: TEST_AMOUNT })
        ).to.be.revertedWith("Unable to accept value: lottery already finished");
    });

    it("sumbit(): Should emit LotteryPayout event", async () => {
        await chainSleep(LOTTERY_TIMEOUT);

        await lotteryInstance.connect(participantOne).submit({ value: TEST_AMOUNT });

        await expect(
            lotteryInstance.connect(participantTwo).submit()
        ).to
            .emit(lotteryInstance, "LotteryPayout")
            .withArgs(participantOne.address, TEST_AMOUNT);
    });

    it("sumbit(): Should payout to winner", async function () {
        const participants = accounts.slice(0, PARTICIPANTS_TOTAL);
        const lastParticipant = participants.pop();

        for (let i = 0; i < participants.length; i++) {
            await lotteryInstance.connect(participants[i]).submit({ value: TEST_AMOUNT });
        }

        await chainSleep(LOTTERY_TIMEOUT);

        await lotteryInstance.connect(lastParticipant).submit({ value: TEST_AMOUNT });

        const startBefore = parseInt(await lotteryInstance.startTimestamp());
        const finishBefore = parseInt(await lotteryInstance.finishTimestamp());

        expect(await provider.getBalance(lotteryInstance.address)).to.equal(TEST_AMOUNT.mul(PARTICIPANTS_TOTAL));

        expect(await lotteryInstance.state()).to.equal(1);
        expect(await lotteryInstance.rngRequestId()).to.not.equal(ZERO_BYTES32);
        expect(finishBefore).to.be.greaterThan(startBefore);
        expect(finishBefore - startBefore).to.equal(LOTTERY_DURATION);

        await mockRngInstance.setValue(await lotteryInstance.rngRequestId(), 10);

        await lotteryInstance.connect(participantTwo).submit();

        const startAfter = parseInt(await lotteryInstance.startTimestamp());
        const finishAfter = parseInt(await lotteryInstance.finishTimestamp());

        expect(await provider.getBalance(lotteryInstance.address)).to.equal(0);

        expect(await lotteryInstance.state()).to.equal(0);
        expect(await lotteryInstance.rngRequestId()).to.equal(ZERO_BYTES32);
        expect(finishAfter).to.be.greaterThan(startAfter);
        expect(finishAfter - startAfter).to.equal(LOTTERY_DURATION);

        expect(startAfter).to.be.greaterThan(startBefore);
        expect(finishAfter).to.be.greaterThan(finishBefore);

        for (let i = 0; i < participants.length; i++) {
            await lotteryInstance.connect(participants[i]).submit({ value: TEST_AMOUNT });
        }

        await chainSleep(LOTTERY_TIMEOUT);

        await lotteryInstance.connect(lastParticipant).submit({ value: TEST_AMOUNT });

        expect(await provider.getBalance(lotteryInstance.address)).to.equal(TEST_AMOUNT.mul(PARTICIPANTS_TOTAL));

        expect(await lotteryInstance.state()).to.equal(1);
        expect(await lotteryInstance.rngRequestId()).to.not.equal(ZERO_BYTES32);
        expect(finishBefore).to.be.greaterThan(startBefore);
        expect(finishBefore - startBefore).to.equal(LOTTERY_DURATION);

        await mockRngInstance.setValue(await lotteryInstance.rngRequestId(), BigNumber.from(2).pow(256).sub(1));

        const lastTx = await lotteryInstance.connect(participantOne).submit()
        const block = await getCurrentBlock();

        await expect(lastTx).to
            .emit(lotteryInstance, "LotteryPayout")
            .withArgs(participantOne.address, TEST_AMOUNT.mul(PARTICIPANTS_TOTAL))

        await expect(lastTx).to
            .emit(lotteryInstance, "LotteryStart")
            .withArgs(block.timestamp, LOTTERY_DURATION);
    });

    it("receive(): Should add ether to contract balance via direct transaction to contract address", async () => {
        await participantOne.sendTransaction({
            to: lotteryInstance.address,
            value: TEST_AMOUNT
        });

        expect(await provider.getBalance(lotteryInstance.address)).to.equal(TEST_AMOUNT);
    });

    it("stakeOf(): Should sum different ether values of a user in a single lottery", async () => {
        await participantOne.sendTransaction({
            to: lotteryInstance.address,
            value: TEST_AMOUNT
        });

        await participantOne.sendTransaction({
            to: lotteryInstance.address,
            value: TEST_AMOUNT
        });

        await participantTwo.sendTransaction({
            to: lotteryInstance.address,
            value: TEST_AMOUNT
        });

        expect(await lotteryInstance.stakeOf(participantOne.address)).to.equal(TEST_AMOUNT.mul(2));
        expect(await lotteryInstance.stakeOf(participantTwo.address)).to.equal(TEST_AMOUNT);
    });

    it("setDuration(): Should revert with message - Lottery duration must be positive", async () => {
        await expect(
            lotteryInstance.connect(owner).setDuration(0)
        ).to.be.revertedWith("Lottery duration must be positive");
    });

    it("setDuration(): Should revert with message - Ownable: caller is not the owner", async () => {
        await expect(
            lotteryInstance.connect(participantOne).setDuration(LOTTERY_DURATION)
        ).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it("setDuration(): Should set duration and emit event NewDuration", async () => {
        await expect(
            lotteryInstance.connect(owner).setDuration(LOTTERY_DURATION)
        ).to
            .emit(lotteryInstance, "NewDuration")
            .withArgs(LOTTERY_DURATION);

        expect(await lotteryInstance.duration()).to.equal(LOTTERY_DURATION);
    });
});
